#!/usr/bin/env bash
# exec from project root

CONTAINERS="nginx mariadb workspace phpmyadmin"

cd laradock

docker-compose up -d ${CONTAINERS}
#docker-compose up -d --build --no-cache ${CONTAINERS}

cd ..
