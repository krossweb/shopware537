#!/usr/bin/env bash
# exec from project root
# exec 'bash scripts/docker_workspace.sh laradock' for logging in as laradock user

cd laradock

if [ $# -eq 1 ]
  then
    docker-compose exec --user=$1 workspace bash
fi
if [ $# -eq 0 ]
  then
    docker-compose exec workspace bash
fi

cd ..
