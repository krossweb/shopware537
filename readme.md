#Shopware DevEnv


##Info

This a testing environment for shopware based on [laradock](https://laradock.io/documentation/). It is intended to be
used on Windows 10 development machines.

##Install

To install Docker on Windows follow this [link](https://docs.docker.com/docker-for-windows/).

Then simply clone this repo and execute these commands:

`cd laradock`

`docker-compose up -d nginx mariadb workspace phpmyadmin`

Now visit [localhost](http://localhost/) and install your demo shop.

##PowerShell Commands

**Enter workspace as root**

`docker exec -it {workspace-container-id} bash`

**Enter workspace as laradock user**

`docker-compose exec --user=laradock workspace bash (TODO: Win10)`

**Show running containers**

`docker-compose ps`

**Close all (or one) container(s)**

`docker-compose stop [container-name]`

**Delete all containers**

`docker-compose down`

**Rebuild containers**

`docker-compose build --no-cache nginx mariadb workspace phpmyadmin`
